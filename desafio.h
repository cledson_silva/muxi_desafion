#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
//autor: Cledson da Silva Araujo 19/02/2021 
// Obs.: Em sistemas embarcador, provavelmente, a quantidade de memória usada é muito importante
// então o uso de bibliotecas externas não seria interessante
// Porém, nesse caso, eu usarei a string.h para ultilizar a função "strlen()"
// Essa poderia ser substituida por um for para achar  o ultimo caracter da string ("\0"), mas de inicio
// Vou usar a função, e em situações similares também ultilizarei funções já prontas
///Questão 1
char* InverteString(char* string)
{
    int tamanho = strlen(string);
    int j = 0;
    char* stringInvertida = (char*)malloc(sizeof(char) * tamanho);
    for (int i = tamanho - 1; i >= 0; i--)
    {
        stringInvertida[j] = string[i];
        j++;
    }
    return stringInvertida;
}
///Questão 2
//A funcão retorna 1 caso a solução seja válida do contrario (retorno == 0) é inválida;
//TODO  criar as funçoes  CopiaVetor,VerificaSeEstaNoVetor & ApagaDoVetor que auxiliarão nesse problema

int EhSolucaoValida(int matriz[9][9])
{


    int tamanho = 9;
    int vet[] = { 1,2,3,4,5,6,7,8,9 };
    int k = 0;
    int ehValido = 1;
    int* aux = (int*)malloc(sizeof(int) * tamanho);
    int estaNoVetor = 0;

    //Verifica repeticoes nas colunas
    for (int i = 0; i < tamanho && ehValido; i++)
    {
        ///copiando o vetor
        for (k = 0; k < tamanho; k++)
        {
            aux[k] = vet[k];
        }
        for (int j = 0; j < tamanho && ehValido; j++)
        {

            ///verificando se o valor da matriz esta no vetor
            for (k = 0; k < tamanho; k++)
            {
                if (aux[k] == matriz[i][j])
                {
                    estaNoVetor = 1;
                    ///"apaga" o vetor colocando o valor -1
                    ///Considerei o valor -1 como invalido
                    aux[k] = -1;
                    break;
                }

            }
            if (estaNoVetor)
            {
                estaNoVetor = 0;
            }
            else
            {
                ehValido = 0;
                break;
            }
        }
    }
    //verifica repeticoes nas linhas

    for (int i = 0; i < tamanho && ehValido; i++)
    {
        ///copiando o vetor
        for (k = 0; k < tamanho; k++)
        {
            aux[k] = vet[k];
        }
        for (int j = 0; j < tamanho && ehValido; j++)
        {

            ///verificando se o valor da matriz esta no vetor
            for (k = 0; k < tamanho; k++)
            {
                if (aux[k] == matriz[j][i])
                {
                    estaNoVetor = 1;
                    ///"apaga" o vetor colocando o valor -1
                    ///Considerei o valor -1 como invalido
                    aux[k] = -1;
                    break;
                }

            }
            if (estaNoVetor)
            {
                estaNoVetor = 0;
            }
            else
            {
                ehValido = 0;
                break;
            }
        }
    }

    return ehValido;
}
///Questão 3

//TODO trasformar comentarios em funções
// considerando length igual o tamanho de "text"
//Obs.: fiz teste com strings como /flamengo/fluminense/botafogo/ e
//a stringSplit não funcionou, acredito que por exceder a memoria dos tipos;
char** stringSplit(const char* text, char separator, int* length)
{

    ///AchaQuantidadeDePalavras
    int numeroDeSeparadores = 0;
    int cont;
    for (int i = 0; i < *length; i++)
    {
        if (text[i] == separator)
        {
            numeroDeSeparadores++;
        }
    }

    int numeroPalavras = numeroDeSeparadores - 1;
    ///Acha a maior palavra para que a matriz possa "acomodar" em memoria  todas as palavras
    int auxMaiorPalavra = 0;
    int maiorPalavra = 0;
    for (int i = 0; i < *length; i++)
    {
        if (text[i] == separator)
        {
            i++;
            if (auxMaiorPalavra > maiorPalavra)
            {
                maiorPalavra = auxMaiorPalavra;
            }
            auxMaiorPalavra = 0;
        }
        auxMaiorPalavra++;
    }

    ///Cria Matriz de Palavras a ser retornada
    char** matrizDePalavras;
    matrizDePalavras = (char**)malloc(sizeof(char*) * numeroPalavras);
    for (int i = 0; i < numeroPalavras; i++)
    {
        matrizDePalavras[i] = (char*)malloc(sizeof(char) * maiorPalavra);

    }
    ///CriaVetorDePalavra
    //"abriu" signigica que ja se encontrou um separador (abriu == 1)
    //ou ainda nao se encontrou nenhum (abriu == 0)
    int abriu = 0;
    //"contadorPalavra" serve para indicar em qual posicao da matriz adicionar o vetor
    int contadorPalavra = 0;
    int posicaoVet = 0;
    char* vet = (char*)malloc(sizeof(char) * maiorPalavra + 1);
    for (int i = 0; i < *length; i++)
    {
        if (text[i] == separator && !abriu)
        {
            abriu = 1;
            i++;
        }

        vet[posicaoVet++] = text[i];
        if (text[i + 1] == separator)
        {
            abriu = 0;

            /// adiciona o vetor na matriz
            for (int j = 0; j < maiorPalavra + 1; j++)
            {
                matrizDePalavras[contadorPalavra][j] = vet[j];
                if (j + 1 == maiorPalavra + 1)
                {
                    matrizDePalavras[contadorPalavra][j] = '\0';
                }
            }

            posicaoVet = 0;
            contadorPalavra++;
        }

    }

    return matrizDePalavras;
}
///Questão 4
//De acordo com o enunciado, entendi que as funções e definiçoes do tipo
//já seriam implementadas e eu poderia ultilizar como eu queira
typedef struct stack Stack;
Stack *stackNew (void);
void stackFree (Stack *p);
void stackPush (Stack *p, int elem);
int stackPop (Stack *p);
int stackEmpty (Stack *p);
///Considerando que stackNew cria uma nova pilha e
///stackPop retorna o valor retirado
/// stackEmpty informa "1" se a pilha estiver vazia ou "0" caso contrario

void stackRemoveEven(Stack* p){
    Stack* aux = stackNew();
    int numeroArmazenado = 0;
    while (!stackEmpty(p)) {
        numeroArmazenado = stackPop(p);
        if (!(numeroArmazenado % 2))///se for impar
        {
            stackPush(aux, numeroArmazenado);
        }
    }
    ///reinverte a pilha
    while(!stackEmpty(aux)) {
        stackPush(p, stackPop(aux));
    }
    stackFree(aux);
}
///Questão 5
//Quando li ordenação lembrei do Quicksort
//Ainda mais com a variavei "x" que representaria um pivo
//No entanto, descobri que o exercicio não era exatamente sobre isso
//Considerei que SingleLinkedListOfIntsNode só teria dois atributos um int com o valor do nó
// e um ponteiro para o proximo nó
typedef struct
{
    int data;
    struct SingleLinkedListOfIntsNode* next;

} SingleLinkedListOfIntsNode;
//Considere a criacao da função abaixo
void insereNoFimdaLista(SingleLinkedListOfIntsNode** lista, SingleLinkedListOfIntsNode* no);
void ordenaLista(SingleLinkedListOfIntsNode* cabeca) {
    SingleLinkedListOfIntsNode* atual = cabeca, * index;
    int aux;

    if (cabeca == NULL) {
        return;
    }
    else {
        while (atual != NULL) {
            
            index = atual->next;

            while (index != NULL) {
            
                if (atual->data > index->data) {
                    aux = atual->data;
                    atual->data = index->data;
                    index->data = aux;
                }
                index = index->next;
            }
            atual = atual->next;
        }
    }

}
SingleLinkedListOfIntsNode* juntaListas(SingleLinkedListOfIntsNode* l1, SingleLinkedListOfIntsNode* l2)
{
    SingleLinkedListOfIntsNode* resultado,*aux;
    aux = l1;
    while(aux->next != NULL)
    {
        aux = aux->next;
    }
    // Na ultima posicao da lista um o valor do proximo no recebe o inicio de l2;
    aux->next = l2;
    // O ponteiro resultado receve o inicio de l1;
    resultado = l1;
    // libero os ponteiro l1 e l2;
    return resultado;

}
void listPartition(SingleLinkedListOfIntsNode** head, int x) {
    ///acha a posiçao x
    int posicaoDeX = 0;
    if ((*head)->next) {
        for (int i = 0; (*head)->next != NULL; i++)
        {
            if ((*head)->data == x) {
                posicaoDeX = i;
            }
        }
    }
    else {
        posicaoDeX = 0;
    }
    ///percorre a lista colocando os valores menores  que x numa outra lista
    SingleLinkedListOfIntsNode* valoresMenores, * aux;
    valoresMenores->next = NULL;
    
    for (aux = head; (aux)->next != NULL; aux = aux->next)
    {
        if ((aux)->data < x) {
            insereNoFimdaLista(&valoresMenores, aux);
        }
    }
    ///percorre a lista colocando os valores maiores ou iguais que x numa outra lista
    SingleLinkedListOfIntsNode* valoresMaiores;
    valoresMaiores->data = NULL;
    for (aux = head; (aux)->next != NULL; aux = aux->next)
    {
        if ((aux)->data >= x) {
            insereNoFimdaLista(&valoresMaiores, aux);
        }
    }
    ///Coloca as duas listas em ordem
    ordenaLista(valoresMaiores);
    ordenaLista(valoresMenores);
    ///junta as duas listas em uma nova lista e aponta a head para o inicio da nova lista
    (*head) = juntaListas(valoresMaiores, valoresMaiores);

}

///Questão 6
//Considerando que resultSize é conhecido
//se um valor se repetir duas vezes em algum dos vetores ele sera retornado
//Se "achouvaloresIguais" err_t.achouValoreIguais == "1" se nao "0"
//Na struct err_t se encontra um ponteiro para o vetor result

typedef struct
{
    int achouValoresIguais;
    int* result;
} err_t;


err_t findCommon(int* list1, int numElem1, int* list2, int numElem2, int* result, int resultSize)
{
    // considereis que result não tinha sido alocada
    //entendi que resultSize , de alguma forma, já saberia quantos numeros o array final teria
    err_t resposta;
    resposta.achouValoresIguais = 0;
    int contResposta = 0;
    result = (int*)malloc(sizeof(int) * resultSize);
    int* listaIguaisAux = (int*)malloc(sizeof(int) * numElem2);
    for (int i = 0; i < numElem2; i++)
    {
        listaIguaisAux[i] = list2[i];
    }
    int numElemAux = numElem2;
    for (int i = 0; i < numElem1; i++)
    {
        for (int j = 0; j < numElemAux; j++)
        {
            if (list1[i] == listaIguaisAux[j])
            {
                result[contResposta] = list1[i];
                contResposta++;
                break;

            }
        }
    }

    //o campo result da struct recebe o endereço do result alocado
    resposta.result = result;
    return resposta;


}
int limpaVetor(int* vet, int tamanho, int num)
{
    int cont = 0;
    //acha o numero de valores nao repetidos
    for (int i = 0; i < tamanho; i++)
    {
        if (vet[i] != num)
        {

            cont++;
        }
    }
    int* vetAux = (int*)malloc(sizeof(int) * cont);
    for (int i = 0; i < cont; i++)
    {
        vetAux[i] = 0;
    }

    int contAux = 0;
    ///preencho o vetor auxiliar com os valores que sobraram
    for (int i = 0; i < tamanho; i++)
    {

        if (vet[i] != num)
        {

            vetAux[contAux] = vet[i];
            contAux++;
        }
    }
    //O endereco de vetAux vai para vet;
    vet = vetAux;

    return cont;
}

